package com.watthanatham.app1

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.watthanatham.app1.databinding.HelloActivityBinding


class HelloActivity : AppCompatActivity() {
    private lateinit var binding: HelloActivityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = HelloActivityBinding.inflate(this.layoutInflater)
        val view = binding.root
        setContentView(view)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val name = intent?.extras?.getSerializable("name").toString()
        binding.stuName.text = name
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}