package com.watthanatham.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import com.watthanatham.app1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(this.layoutInflater)
        val view = binding.root
        setContentView(view)
        findViewById<Button>(R.id.button).setOnClickListener {
            var intent = Intent(this, HelloActivity::class.java)
            var name = binding.stuName.text.toString()
            Log.d("Name :", name)
            intent.putExtra("name", name)
            this.startActivity(intent)
        }
    }
}